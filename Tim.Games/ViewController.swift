//
//  ViewController.swift
//  Tim.Games
//
//  Created by Alain on 16-09-27.
//  Copyright © 2016 Production sur support. All rights reserved.
//
//  ============================================================================================
//  À l'usage exclusif des étudiants et étudiantes de
//  Techniques d'Intégration Multimédia
//  du cégep de Saint-Jérôme.
//  --------------------------------------------------------------------------------------------
//  Il est interdit de reproduire, en tout ou en partie, à des fins commerciales,
//  le code source, les scènes, les éléments graphiques, les classes et
//  tout autre contenu du présent projet sans l’autorisation écrite de l'auteur.
//
//  Pour obtenir l’autorisation de reproduire ou d’utiliser, en tout ou en partie,
//  le présent projet, veuillez communiquer avec:
//
//  Alain Boudreault, aboudrea@cstj.qc.ca, ve2cuy.wordpress.com
//
//  ============================================================================================
//  Version de départ - disponible à https://bitbucket.org/alain_boudreault/tim.ze.games-depart
//  -------------------------------------------------------------
//  Note: voici comment desactiver les messages superflus de la
//        console:
//
// dans Product>>Scheme>>Edit Scheme...>>Run...Arguments -> ajouter cette variable d'environment:
//
//    Name:OS_ACTIVITY_MODE, Value: disable
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource {

    // MARK: - Les 'Outlets' de la classe
    @IBOutlet weak var tailleRequete: UILabel!
    @IBOutlet weak var progression: UIActivityIndicatorView!
    @IBOutlet weak var collectionDesItems: UICollectionView!

    // MARK: - Les 'IBActions' de la classe
    @IBAction func modifierTailleRequete(_ sender: UIStepper) {
        tailleRequete.text = String(format:"%2.0f", sender.value)
    }
    
    /// =====================================================================
    
    // MARK: - Les propriétés de la classe
    private var _résultatDeLaRequête = Dictionary<String, Any>()
    private var _listeDesItems       = Array<Dictionary<String, Any>>()
    /// =====================================================================
    
    // MARK: - Méthodes locales:
    /// =====================================================================
    
    // TODO: 2 - Programmer la méthode obtenirDonnéesVersionBloquante()
    func obtenirDonnéesVersionBloquante(_ chaine:String) {
        print("Exécution de obtenirDonnéesVersionBloquante() avec \(chaine)")
    
    } // obtenirDonnéesVersionBloquante
    
    
    // TODO: 3 - Programmer la méthode afficherDonnées()

    // TODO: 6 - Programmer la méthode obtenirDonnéesVersionNonBloquante()
   
    // ----------------------------------------------------------------
    

    // MARK: - Méthodes de la super classe
    /// =====================================================================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Voici comment créer une structure json à partir d'un dictionnaire:
        let params = ["username":"bob", "password":"secret"]
        let json   = try! JSONSerialization.data(withJSONObject: params, options: [/*.prettyPrinted*/])
        print(json)
        print(json as NSData)
        print(String(data: json, encoding: .utf8)!)
        
        // Voici comment comment initialiser un dictionnaire à partir d'une chaine json: Note: parseJSONString est une extension locale
        let tableau = "{\"nom\":\"Bob\", \"force\":99}".parseJSONString as? Dictionary<String, Any>
        print(tableau)

        // TODO: 1 - Obtenir les données initiales de l'API web
        /// obtenirDonnées("")
        obtenirDonnéesVersionBloquante("TIM")
        
        // TODO: 1.1 - Tester l'accès aux données reçues:
        
    }

    /// =====================================================================
     override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Préparer les données pour le segue vers la scène Détails:
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // TODO: Passer l'item courant à la scène détails
    
    }

    // MARK:- Méthodes de UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // TODO:  4 - Retourner le nombre d'items reçus suite à la requête de l'API
        return 10  // À effacer!!!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellule = collectionView.dequeueReusableCell(withReuseIdentifier: "modeleCellule1", for: indexPath) as! CollectionViewCellPerso1
        cellule.pochetteTitre.text = "jeu no \(indexPath.row)"
        // cellule.contentView.tag = indexPath.row

        // TODO: 5a - Renseigner les éléments d'interface de la cellule courante: titre, image, ...

        // TODO: 5b - Renseigner les éléments d'interface en version non bloquante

        // Renseigner la couleur de l'entête indexPath.row modulo 2
        let couleur1 = UIColor.init(red: 140 / 255.0, green: 188 / 255.0, blue: 220 / 255.0, alpha: 1)
        let couleur2 = UIColor.init(red: 108 / 255.0, green: 145 / 255.0, blue: 168 / 255.0, alpha: 1)
        let couleur = indexPath.row % 2 == 0 ? couleur1 : couleur2
        cellule.entete.backgroundColor = couleur
        
        return cellule
    } // collectionView: cellForItemAt
    
    // MARK: - Méthodes de UITextFieldDelegate
    // *************************************************************************************************
    func textFieldDidBeginEditing(_ textField: UITextField) {
        // print("textFieldDidBeginEditing")
        progression.startAnimating()
    }  // textFieldDidBeginEditing
    
    
    // *************************************************************************************************
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // print("textFieldShouldReturn")
        
        /* *********************************
         À compléter ....
         ********************************* */
        // TODO: Convertir la chaine en format 'escaped' pour le web. Par exemple, ' '= %20
        // let escapedText = textField.text!.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics)!
        
        // print("escapedText = \(escapedText)")
        
        // TODO: Relancer la requete vers l'API
        /// _ = obtenirDonnéesVersionBloquante(escapedText)
        
        //----------------------------------
        textField.resignFirstResponder()
        progression.stopAnimating()
        
        return true
    } // textFieldShouldReturn
    
}

